//: Playground - noun: a place where people can play

import Foundation

let INIT_COST = 2
let COST_FITST_HOUR = 3
let COST_NEXT_HOUR = 4


public func solution(inout E : String, inout _ L : String) -> Int {
    // write your code in Swift 2.2 (Linux)
    var cost = 0
    let eTup = parseTimseString(E)
    let lTup = parseTimseString(L)
    
    let dif = getDiffMinAndHours(eTup, lTup: lTup)
    
    
    
    cost = calculateCost(dif)
    
    
    return cost
}

func parseTimseString(time: String) -> (Int,Int) {
    var components : [String] = time.componentsSeparatedByString(":")
    
    guard let hours =  Int(components[0]), let minutes = Int(components[1]) else {
        fatalError("Wrong format")
    }
    return (hours ,minutes)
    
}


func getDiffMinAndHours(eTup:(Int, Int), lTup: (Int, Int))->(Int, Int){
    
    var hours = lTup.0 - eTup.0
    var minutes = lTup.1 - eTup.1
    
    if minutes < 0 {
        hours -= 1
        minutes += 60
    }
    
    return (hours, minutes)
}

func calculateCost( dif : (hours:Int, minutes:Int))-> Int {
    
    var cost = INIT_COST // initial cost for entrance
   
    
    
    switch (dif) {
    case (0, 0):
        break
    case (0, _):
        cost += COST_FITST_HOUR //less than hour
    case (_, 0):
        cost += COST_FITST_HOUR + (dif.hours-1) * COST_NEXT_HOUR
    case (_, _):
        cost += COST_FITST_HOUR + dif.hours * COST_NEXT_HOUR
   
    }
    
    return cost
}


var E = "09:42", L = "11:42"

solution(&E, &L)
