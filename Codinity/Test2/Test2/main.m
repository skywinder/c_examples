//
//  main.m
//  Test2
//
//  Created by Petr Korolev on 06/07/16.
//  Copyright © 2016 Petr Korolev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Codinity.h"

NSMutableArray *getIntervals(NSArray *array);

int main(int argc, const char *argv[]) {
    @autoreleasepool {
        NSArray *citiesMap = @[@9, @1, @4, @9, @0, @4, @8, @9, @0, @1];
        NSMutableArray *arr = [Codinity getListOfCities: citiesMap];
        NSLog(@"arr = %@", arr);
    }
    return 0;
}



