//
//  Codinity.m
//  Test2
//
//  Created by Petr Korolev on 06/07/16.
//  Copyright © 2016 Petr Korolev. All rights reserved.
//
//
//  Codinity.h
//  Test2
//
//  Created by Petr Korolev on 06/07/16.
//  Copyright © 2016 Petr Korolev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Codinity.h"


@interface City : NSObject
@property(nonatomic, strong) NSMutableArray *linkedCities;

@property NSInteger routeToCapital;

@property(nonatomic) BOOL isCapital;

- (instancetype)initWithLinkedCities:(NSMutableArray *)aLinkedCities;

- (NSString *)description;

+ (instancetype)cityWithLinkedCities:(NSMutableArray *)aLinkedCities;

+ (NSInteger)findRuote:(NSMutableArray *)array from:(NSNumber *)from to:(NSNumber *)to;

@end

void fillRoutes(NSMutableArray *array, NSNumber *number, int route) {

    City *city = array[(NSUInteger) number.intValue];

    if (city.routeToCapital > 0 )
        return;

    else city.routeToCapital = route + 1;

    for (NSNumber *linkedCity in city.linkedCities) {
        fillRoutes(array, linkedCity, route + 1);
    }


}


@implementation City : NSObject


- (instancetype)initWithLinkedCities:(NSMutableArray *)aLinkedCities {
    self = [super init];
    if (self) {
        self.linkedCities = aLinkedCities;
        self.isCapital = NO;
        self.routeToCapital = -1;
    }

    return self;
}

- (NSString *)description {
    NSMutableString *description = [NSMutableString stringWithFormat:@"<%@: ", NSStringFromClass([self class])];
    [description appendFormat:@", self.routeToCapital=%li", self.routeToCapital];

    return description;
}


+ (instancetype)cityWithLinkedCities:(NSMutableArray *)aLinkedCities {
    return [[self alloc] initWithLinkedCities:aLinkedCities];
}

+ (NSInteger)findRuote:(NSMutableArray *)array from:(NSNumber *)from to:(NSNumber *)to {


    NSMutableArray *a = array.mutableCopy;
    if ([from isEqualToNumber:to]) {
        return 0;
    }

    City *city = [City getCityByIndex:from arr:a];

    NSInteger routeToCapital = city.routeToCapital;

    switch (routeToCapital) {
        case 0:
            return 0;
        case -1:
            city.routeToCapital = -2;

            for (NSNumber *number in city.linkedCities) {
                NSInteger res = [City findRuote:a from:number to:to];
                if (res >= 0) {
                    city.routeToCapital = res + 1;
                    return res + 1;
                }
            }
            return -2;
        case -2:
            return -2;

        default:
            return routeToCapital;
    }
}

+ (City *)getCityByIndex:(NSNumber *)number arr:(NSMutableArray *)arr {
    return arr[(NSUInteger) number.intValue];
}

@end


@implementation Codinity


+ (NSMutableArray *)getListOfCities:(NSArray *)T {

    NSMutableArray *res = [[NSMutableArray alloc] initWithCapacity:T.count];
    for (NSNumber *number in T) {
        City *city = [City cityWithLinkedCities:[NSMutableArray arrayWithArray:@[number]]];
        [res addObject:city];
    }
    NSNumber *capital;

    for (NSUInteger i = 0; i < res.count; ++i) {
        City *city = res[i];

        NSUInteger dest = [city.linkedCities[0] unsignedIntegerValue];
        if (dest == i) {
            city.isCapital = YES;
            [city.linkedCities removeObject:@(i)];
            city.routeToCapital = 0;
            capital = @(i);
        } else {
            City *city2 = res[dest];
            [city2.linkedCities addObject:@(i)];

        }


    }

    fillRoutes(res, capital, 0);

    NSLog(@"res = %@", res);
//
//    NSMutableArray *routes = [[NSMutableArray alloc] init];
//    for (int j = 0; j < res.count; ++j) {
//        [routes addObject:@([City findRuote:res from:@(j) to:capital])];
//
//    }

    NSMutableArray *occur = [[NSMutableArray alloc] init];
//    for (int k = 1; k < routes.count; ++k) {
//
//        NSInteger answer = 0;
//        for (NSNumber *number in routes) {
//            if (number.intValue == k)
//                answer++;
//        }
//
//        [occur addObject:@(answer)];
//
//    }
    return occur;
}


@end



