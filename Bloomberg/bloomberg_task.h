#import <Foundation/Foundation.h>

NSInteger minNumberOfCoins(NSInteger amount)
{
    NSArray *availableCoins = @[@1, @5, @10, @20, @50, @100];

    if ( amount < 0 )
    {
        return 0;
    }

    // TODO: Calculate minimum number of coins required based on the amount provided
    NSInteger restAmount = amount;
    NSInteger coins = 0;

    for (NSUInteger i = availableCoins.count - 1; i >= 0; i--)
    {
        NSNumber *currentCoinNum = availableCoins[i];
        int currentCoin = currentCoinNum.intValue;
        // printf("%d\n", currentCoin);
        coins += restAmount / currentCoin;
        restAmount = restAmount % currentCoin;
    }

    return coins;
}

// ------ Do not modify below this line ------

void processAmount(NSInteger amount)
{
    printf("Minimum number of coins for %ld is %ld\n", amount, minNumberOfCoins(amount));
}

int main(int argc, char *argv[])
{
    @autoreleasepool
    {
        processAmount(150); // 2
        processAmount(95);
        processAmount(267);
        processAmount(999);

        processAmount(0);
        processAmount(-10);
    }
}