//
// Created by Petr Korolev on 18/06/15.
// Copyright (c) 2015 SW. All rights reserved.
//


#ifndef __Sqrt_H_
#define __Sqrt_H_


class Solution
{
public:
    int mySqrt(int x);
};


#endif //__Sqrt_H_
