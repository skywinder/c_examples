//
// Created by Petr Korolev on 18/06/15.
// Copyright (c) 2015 SW. All rights reserved.
//

#include "Solution.h"
#include <limits.h>


int Solution::mySqrt(int x)
{
    int ans = 0;
    if ( x == 0 ) return 0;
    int left = 1, right = x;
    while (left <= right)
    {
        int mid = left + ( right - left ) / 2;
        if ( mid < x / mid )
        {
            left = mid + 1;
            ans = mid;

        } else
        {
            right = mid - 1;
        }
    }
    return ans;
}
