//
//  Introduction.cpp
//  hackerrank
//
//  Created by Petr Korolev on 17/08/2016.
//  Copyright © 2016 Petr Korolev. All rights reserved.
//

#include "Introduction.hpp"

#include <iostream>
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

void in_out();

void basic_data_types();

void loop() ;

using namespace std;

void _main() {
//    in_out();
    loop();
    return;
}

void in_out() {
    int n = 3;

    int sum = 0;
    for (int i = 0; i < n; ++i) {
        int tmp = 0;
        cin >> tmp;
        sum += tmp;
    }

    cout << sum << endl;
}

void basic_data_types() {

// 3 444 12345678912345 a 334.23 14049.30493

    int d = 0;
    long ld = 0;
    long long lld = 0;
    char c = 'c';
    float f = 0.0;
    double lf = 0.0;

    const char *in_format = "%d %ld %lld %c %f %lf";
    const char *out_format = "%d\n%ld\n%lld\n%c\n%.3f\n%.9lf";

    scanf(in_format, &d, &ld, &lld, &c, &f, &lf);
    printf(out_format, d, ld, lld, c, f, lf);
}


void loop() {
    int from, to;
    string num[10] = {"Greater than 9", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};

    cin >> from;
    cin >> to;

    for (int i = from; i < to + 1; ++i) {

        if (i > 9) {
             cout << (i % 2 == 0 ? "even" : "odd") << endl;
        }
        else {
            cout << num[i] << endl;
        }
    }
    return;
}
