//
// Created by Petr Korolev on 18/06/15.
// Copyright (c) 2015 SW. All rights reserved.
//


#ifndef __Shape_H_
#define __Shape_H_


#include <stdio.h>
#include <iostream>



class Shape
{
protected:
    int size;

public:
    int getSize() const
    {
        return size;
    }

    void setSize(int size)
    {
        Shape::size = size;
    }

    virtual ~Shape()
    {
        printf("~Shape\n");

    }

    Shape()
    {
        printf("Shape\n");
        size = 1;
    }

    Shape(int s)
    {
        printf("Shape(%d)\n", s);
        size = s;
    }

    void printSize()
    {
        printf("size = %d", size);
    }

    virtual double area()=0;

    void printarea()
    {std::cout << this->area() << '\n'; }

};

class Circle: public Shape {
protected:
public:

    virtual double area() override;

    Circle(int s) : Shape(s)
    {
        printf("Curcle(%d)\n", s);
    }

    Circle() : Shape()
    {
        printf("Circle()\n");
    }

    virtual ~Circle()
    {
        printf("~Circle\n");
    }

    int radius() const
    {
        return size;
    }


    void setRadius(int radius)
    {
        Circle::size = radius;
    }


public:

};

void runTests();

#endif //__Shape_H_
