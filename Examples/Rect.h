//
// Created by Petr Korolev on 18/06/15.
// Copyright (c) 2015 SW. All rights reserved.
//


#ifndef __Rect_H_
#define __Rect_H_


#include "Shape.h"

class Rect : public Shape
{

protected:
    int a;
    int b;

public:
    Rect(int s):Shape(s){
        printf("Rect(%d)\n", s);
        a = s;
        b = s;
    }

    double area(){
        return a * b;
    }
};


#endif //__Rect_H_
