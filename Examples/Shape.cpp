//
// Created by Petr Korolev on 18/06/15.
// Copyright (c) 2015 SW. All rights reserved.
//

#include "Shape.h"
#include "Rect.h"

void runTests()
{
//    Shape shape1;
    Circle circle(2);
    Rect rect(6);
    Shape * shape = &circle;
    Shape * shape2 = &rect;

//    std::cout << circle.area() << "\n";
//    std::cout << shape->area( )<< "\n";
//    std::cout << rect.area( )<< "\n";

    shape->printarea();
    shape2->printarea();
}

double Circle::area()
{
    return 3.14 * radius() * radius();
}
