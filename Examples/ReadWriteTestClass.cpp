//
// Created by Petr Korolev on 18/06/15.
// Copyright (c) 2015 SW. All rights reserved.
//

#include <iostream>
#include "ReadWriteTestClass.h"

void readAndPrintTest()
{
    std::cout << "Hello, World!\n";

    char str[109];
//    gets(str);
    scanf("%s", str);
    puts("Here is a:");
    puts(str);
}

void intRead()
{
    int a;

    printf("Please input an integer value: \n");
    scanf("%d", &a);
    printf("You entered: %d\n", a);
}